package ru.tsc.gavran.tm.command.system;

import ru.tsc.gavran.tm.command.AbstractCommand;
import ru.tsc.gavran.tm.util.NumberUtil;

public class InfoDisplayCommand extends AbstractCommand {

    @Override
    public String name() {
        return "info";
    }

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return "Display system information.";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

}