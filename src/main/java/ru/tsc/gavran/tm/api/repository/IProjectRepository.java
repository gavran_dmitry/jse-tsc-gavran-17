package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    boolean existsById(String id);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);

    Project startById(String id);

    Project startByName(String name);

    Project startByIndex(int index);

    Project finishById(String id);

    Project finishByName(String name);

    Project finishByIndex(int index);

    Project changeStatusById(String id, Status status);

    Project changeStatusByName(String name, Status status);

    Project changeStatusByIndex(int index, Status status);

    void clear();

}