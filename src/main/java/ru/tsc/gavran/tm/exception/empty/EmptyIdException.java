package ru.tsc.gavran.tm.exception.empty;

import ru.tsc.gavran.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id is empty");
    }

    public EmptyIdException(String value) {
        super("Error." + value + " Id is empty");
    }

}
