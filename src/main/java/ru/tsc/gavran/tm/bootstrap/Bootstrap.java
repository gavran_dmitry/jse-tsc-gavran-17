package ru.tsc.gavran.tm.bootstrap;

import ru.tsc.gavran.tm.api.repository.*;
import ru.tsc.gavran.tm.api.service.*;
import ru.tsc.gavran.tm.command.AbstractCommand;
import ru.tsc.gavran.tm.command.project.*;
import ru.tsc.gavran.tm.command.system.*;
import ru.tsc.gavran.tm.command.task.*;
import ru.tsc.gavran.tm.exception.system.UnknownCommandException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.repository.*;
import ru.tsc.gavran.tm.service.CommandService;
import ru.tsc.gavran.tm.service.ProjectService;
import ru.tsc.gavran.tm.service.ProjectTaskService;
import ru.tsc.gavran.tm.service.TaskService;
import ru.tsc.gavran.tm.util.TerminalUtil;
import ru.tsc.gavran.tm.api.service.ILogService;
import ru.tsc.gavran.tm.service.LogService;
import ru.tsc.gavran.tm.api.service.IServiceLocator;

public class Bootstrap implements IServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    public void start(String[] args) {
        displayWelcome();
        initData();
        parseArgs(args);
        process();
    }

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());
        registry(new ArgumentsDisplayCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectShowListCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    private void initData() {
        projectService.add(new Project("Project C", "1"));
        projectService.add(new Project("Project A", "2"));
        projectService.add(new Project("Project B", "3"));
        projectService.add(new Project("Project D", "4"));
        taskService.add(new Task("Task C", "1"));
        taskService.add(new Task("Task A", "2"));
        taskService.add(new Task("Task B", "3"));
        taskService.add(new Task("Task D", "4"));
        projectService.finishByName("Project B");
        projectService.startByName("Project A");
        taskService.finishByName("Task C");
        taskService.startByName("Task B");
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void process() {
        logService.debug("Logging started.");
        String exit = new ExitCommand().name();
        String command = "";
        while (!exit.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                logService.info("Completed.");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    public void parseArg(final String arg) {
        AbstractCommand arguments = commandService.getCommandByArg(arg);
        if (arguments == null) throw new UnknownCommandException();
        arguments.execute();
    }

    public void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }
}